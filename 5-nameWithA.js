const { got } = require("./data/data-1_1");

function nameWithA(got) {
  // your code goes here
  if (typeof got == "object") {
    let names = got["houses"]
      .map((house) => {
        return house.people.map((people) => {
          return people.name;
        });
      })
      .flat(1)
      .filter((name) => name.includes("a") || name.includes("A"));
    return names;
  } else {
    return null;
  }
}

module.exports = { nameWithA };
