const { got } = require("./data/data-1_1");

function surnameWithA(got) {
  // your code goes here
  if (typeof got == "object") {
    let Allnames = got["houses"]
      .map((house) => {
        return house.people.map((people) => {
          return people.name;
        });
      })
      .flat(1);
      Allnames=Allnames.map((name)=>name.split(" "))
      let surnameWithA=Allnames.filter((name)=>(name[1].startsWith('A'))).map((name)=>name.join(" "))
      return surnameWithA
  } else {
    return null;
  }
}

module.exports={surnameWithA}