const { got } = require("./data/data-1_1");

function peopleNameOfAllHouses(got) {
  // your code goes here
  if (typeof got == "object") {
    let allHousesPeopleName = {};

    Object.keys(got["houses"]).forEach((house) => {
      if (!allHousesPeopleName[got["houses"][house]["name"]]) {
        allHousesPeopleName[got["houses"][house]["name"]] = [];
      }
      got["houses"][house]["people"].forEach((people) => {
        allHousesPeopleName[got["houses"][house]["name"]].push(people.name);
      });
    });

    return allHousesPeopleName;
  } else {
    return null;
  }
}

module.exports = { peopleNameOfAllHouses };
