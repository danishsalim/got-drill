const { got } = require("./data/data-1_1");

function everyone(got) {
  // your code goes here
  if (typeof got == "object") {
    let names = got["houses"]
      .map((house) => {
        return house.people.map((people) => {
          return people.name;
        });
      })
      .flat(1);
    return names;
  } else {
    return null;
  }
}

module.exports = { everyone };
