const { got } = require("./data/data-1_1");

function nameWithS(got) {
  // your code goes here
  if (typeof got == "object") {
    let names = got["houses"]
      .map((house) => {
        return house.people.map((people) => {
          return people.name;
        });
      })
      .flat(1)
      .filter((name) => name.includes("s") || name.includes("S"));
    return names;
  } else {
    return null;
  }
}

module.exports = { nameWithS };
