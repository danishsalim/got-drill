function countAllPeople(got) {
  // your code goes here
  let totalPeople = 0;
  Object.keys(got).forEach((houses) => {
    got[houses].forEach((house) => {
      totalPeople += house.people.length;
    });
  });

  return totalPeople;
}

module.exports = { countAllPeople };
