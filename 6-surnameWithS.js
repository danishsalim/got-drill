const { got } = require("./data/data-1_1");

function surnameWithS(got) {
  // your code goes here
  if (typeof got == "object") {
    let Allnames = got["houses"]
      .map((house) => {
        return house.people.map((people) => {
          return people.name;
        });
      })
      .flat(1);
      Allnames=Allnames.map((name)=>name.split(" "))
      let surnameWithS=Allnames.filter((name)=>(name[1].startsWith('S'))).map((name)=>name.join(" "))
      return surnameWithS
  } else {
    return null;
  }
}

module.exports={surnameWithS}
