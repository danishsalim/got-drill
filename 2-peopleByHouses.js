const { got } = require("./data/data-1_1");

function peopleByHouses(got) {
  // your code goes here
  if (typeof got == "object") {
    let peopleByHouses;
    //using forech to iterate each houses and then using reduce to get number of people
    Object.keys(got).forEach((houses) => {
      peopleByHouses = got[houses].reduce((people, house) => {
        people[house.name] = house.people.length;
        return people;
      }, {});
    });
    return peopleByHouses;
  } else {
    return null;
  }
}

module.exports = { peopleByHouses };
